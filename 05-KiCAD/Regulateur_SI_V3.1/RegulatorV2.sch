EESchema Schematic File Version 4
LIBS:RegulatorV2-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "SWACC - Small Wind Charge Controller"
Date "2018-12-10"
Rev "3.2"
Comp "Tripalium"
Comment1 "Design & Schematic : Adrien Prévost - adriprevost@hotmail.fr"
Comment2 "Contributors : Adrien Prévost, Clément Gangneux, Bastien Gary, Jay Hudnall"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Controller:SG3525 IC1
U 1 1 5ACE3170
P 4650 7150
F 0 "IC1" H 4700 7200 60  0000 C CNN
F 1 "SG3525" H 18800 400 60  0000 C CNN
F 2 "Housings_DIP:DIP-16_W7.62mm" H 18800 400 60  0001 C CNN
F 3 "" H 18800 400 60  0000 C CNN
	1    4650 7150
	1    0    0    -1  
$EndComp
Text Label 7450 1300 0    60   ~ 0
GND
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5ACF5A5D
P 7450 1300
F 0 "#FLG01" H 7450 1375 50  0001 C CNN
F 1 "PWR_FLAG" H 7450 1450 50  0001 C CNN
F 2 "" H 7450 1300 50  0001 C CNN
F 3 "" H 7450 1300 50  0001 C CNN
	1    7450 1300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5ACF5AF4
P 1400 1350
F 0 "#FLG02" H 1400 1425 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 1500 50  0000 C CNN
F 2 "" H 1400 1350 50  0001 C CNN
F 3 "" H 1400 1350 50  0001 C CNN
	1    1400 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5ACF5BE1
P 6300 1800
F 0 "R18" V 6380 1800 50  0000 C CNN
F 1 "2.2k" V 6300 1800 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6230 1800 50  0001 C CNN
F 3 "" H 6300 1800 50  0001 C CNN
	1    6300 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 5ACF5CC4
P 7000 1400
F 0 "R19" V 7080 1400 50  0000 C CNN
F 1 "2.2k" V 7000 1400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6930 1400 50  0001 C CNN
F 3 "" H 7000 1400 50  0001 C CNN
	1    7000 1400
	0    1    1    0   
$EndComp
Text Label 8500 1600 2    60   ~ 0
Sensor_VDD
$Comp
L power:GND #PWR03
U 1 1 5ACF5E12
P 6150 1800
F 0 "#PWR03" H 6150 1550 50  0001 C CNN
F 1 "GND" H 6150 1650 50  0000 C CNN
F 2 "" H 6150 1800 50  0001 C CNN
F 3 "" H 6150 1800 50  0001 C CNN
	1    6150 1800
	0    1    1    0   
$EndComp
Text Label 8100 2850 0    60   ~ 0
Vout_sensor
Text Label 1500 1650 0    60   ~ 0
Vin
$Comp
L Device:D_Zener D1
U 1 1 5AD074C8
P 3550 2350
F 0 "D1" H 3550 2450 50  0000 C CNN
F 1 "1N5353B" H 3550 2250 50  0000 C CNN
F 2 "Diodes_THT:D_DO-15_P12.70mm_Horizontal" H 3550 2350 50  0001 C CNN
F 3 "" H 3550 2350 50  0001 C CNN
	1    3550 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5AD076BE
P 3550 1950
F 0 "R9" V 3630 1950 50  0000 C CNN
F 1 "1.5k" V 3550 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_Power_L38.0mm_W9.0mm_P45.72mm" V 3480 1950 50  0001 C CNN
F 3 "" H 3550 1950 50  0001 C CNN
	1    3550 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 5AD07701
P 3950 2150
F 0 "Q1" H 4150 2200 50  0000 L CNN
F 1 "FJA43100TU" H 4150 2100 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-3PB__Vertical" H 4150 2250 50  0001 C CNN
F 3 "" H 3950 2150 50  0001 C CNN
	1    3950 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5AD07769
P 4300 2500
F 0 "C1" H 4325 2600 50  0000 L CNN
F 1 "100n" H 4325 2400 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 4338 2350 50  0001 C CNN
F 3 "" H 4300 2500 50  0001 C CNN
	1    4300 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5AD07877
P 3950 2650
F 0 "#PWR04" H 3950 2400 50  0001 C CNN
F 1 "GND" H 3950 2500 50  0000 C CNN
F 2 "" H 3950 2650 50  0001 C CNN
F 3 "" H 3950 2650 50  0001 C CNN
	1    3950 2650
	1    0    0    -1  
$EndComp
Text Label 3850 1700 0    60   ~ 0
Vin
Text Label 4700 2350 0    60   ~ 0
Supply
Text Label 7900 2250 2    60   ~ 0
Supply
$Comp
L Device:R R5
U 1 1 5AD07CD3
P 2050 4600
F 0 "R5" V 2130 4600 50  0000 C CNN
F 1 "20k" V 2050 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1980 4600 50  0001 C CNN
F 3 "" H 2050 4600 50  0001 C CNN
	1    2050 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5AD07D52
P 2050 5050
F 0 "R6" V 2130 5050 50  0000 C CNN
F 1 "11k" V 2050 5050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1980 5050 50  0001 C CNN
F 3 "" H 2050 5050 50  0001 C CNN
	1    2050 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5AD07DFB
P 2350 5050
F 0 "R11" V 2430 5050 50  0000 C CNN
F 1 "4.3k" V 2350 5050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2280 5050 50  0001 C CNN
F 3 "" H 2350 5050 50  0001 C CNN
	1    2350 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5AD07E58
P 2650 4600
F 0 "R13" V 2730 4600 50  0000 C CNN
F 1 "105k" V 2650 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2580 4600 50  0001 C CNN
F 3 "" H 2650 4600 50  0001 C CNN
	1    2650 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5AD07EAA
P 2650 5050
F 0 "R14" V 2730 5050 50  0000 C CNN
F 1 "10.2k" V 2650 5050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2580 5050 50  0001 C CNN
F 3 "" H 2650 5050 50  0001 C CNN
	1    2650 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5AD08023
P 2350 5400
F 0 "#PWR05" H 2350 5150 50  0001 C CNN
F 1 "GND" H 2350 5250 50  0000 C CNN
F 2 "" H 2350 5400 50  0001 C CNN
F 3 "" H 2350 5400 50  0001 C CNN
	1    2350 5400
	1    0    0    -1  
$EndComp
Text Label 1200 4350 0    60   ~ 0
Vin
$Comp
L RegulatorV2-rescue:SW_SP3T SW1
U 1 1 5AD086E0
P 3750 4800
F 0 "SW1" H 3750 5000 50  0000 C CNN
F 1 "JS203011CQN" H 3750 4600 50  0000 C CNN
F 2 "Switchs_perso:SW_SP3T" H 3125 4975 50  0001 C CNN
F 3 "" H 3125 4975 50  0001 C CNN
	1    3750 4800
	-1   0    0    1   
$EndComp
Text Label 3000 4900 0    60   ~ 0
12V
Text Label 3200 4800 0    60   ~ 0
24V
Text Label 3050 4700 0    60   ~ 0
48V
$Comp
L RegulatorV2-rescue:LM358 U1
U 1 1 5AD08D23
P 7550 1900
F 0 "U1" H 7550 2100 50  0000 L CNN
F 1 "LM358N" H 7550 1700 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 7550 1900 50  0001 C CNN
F 3 "" H 7550 1900 50  0001 C CNN
	1    7550 1900
	1    0    0    1   
$EndComp
Text Label 6750 2000 0    60   ~ 0
Vref
$Comp
L Device:R R20
U 1 1 5AD09DFB
P 8800 1600
F 0 "R20" V 8880 1600 50  0000 C CNN
F 1 "10k" V 8800 1600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8730 1600 50  0001 C CNN
F 3 "" H 8800 1600 50  0001 C CNN
	1    8800 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5AD09E57
P 8800 2000
F 0 "R21" V 8880 2000 50  0000 C CNN
F 1 "10k" V 8800 2000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8730 2000 50  0001 C CNN
F 3 "" H 8800 2000 50  0001 C CNN
	1    8800 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5AD09EB5
P 8800 2250
F 0 "#PWR06" H 8800 2000 50  0001 C CNN
F 1 "GND" H 8800 2100 50  0000 C CNN
F 2 "" H 8800 2250 50  0001 C CNN
F 3 "" H 8800 2250 50  0001 C CNN
	1    8800 2250
	1    0    0    -1  
$EndComp
$Comp
L RegulatorV2-rescue:LM358 U1
U 2 1 5AD09F11
P 9750 1650
F 0 "U1" H 9750 1850 50  0000 L CNN
F 1 "LM358N" H 9850 1750 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 9750 1650 50  0001 C CNN
F 3 "" H 9750 1650 50  0001 C CNN
	2    9750 1650
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5AD0A2DC
P 9650 1250
F 0 "#PWR07" H 9650 1000 50  0001 C CNN
F 1 "GND" H 9650 1100 50  0000 C CNN
F 2 "" H 9650 1250 50  0001 C CNN
F 3 "" H 9650 1250 50  0001 C CNN
	1    9650 1250
	1    0    0    1   
$EndComp
Text Label 9000 1250 2    60   ~ 0
Vref
$Comp
L Device:R R23
U 1 1 5AD0A421
P 9300 1550
F 0 "R23" V 9380 1550 50  0000 C CNN
F 1 "10k" V 9300 1550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9230 1550 50  0001 C CNN
F 3 "" H 9300 1550 50  0001 C CNN
	1    9300 1550
	0    1    1    0   
$EndComp
Text Label 9650 2200 0    60   ~ 0
Supply
$Comp
L power:GND #PWR08
U 1 1 5AD0AC8F
P 9150 1550
F 0 "#PWR08" H 9150 1300 50  0001 C CNN
F 1 "GND" H 9150 1400 50  0000 C CNN
F 2 "" H 9150 1550 50  0001 C CNN
F 3 "" H 9150 1550 50  0001 C CNN
	1    9150 1550
	0    1    1    0   
$EndComp
Text Label 10750 1650 2    60   ~ 0
Sensor_VSS
$Comp
L Device:R R15
U 1 1 5AD0B620
P 3000 7350
F 0 "R15" V 3080 7350 50  0000 C CNN
F 1 "47k" V 3000 7350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2930 7350 50  0001 C CNN
F 3 "" H 3000 7350 50  0001 C CNN
	1    3000 7350
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 5AD0B6AE
P 3550 7550
F 0 "R17" V 3630 7550 50  0000 C CNN
F 1 "22" V 3550 7550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3480 7550 50  0001 C CNN
F 3 "" H 3550 7550 50  0001 C CNN
	1    3550 7550
	0    1    1    0   
$EndComp
$Comp
L Device:C CT1
U 1 1 5AD0B731
P 2700 7900
F 0 "CT1" H 2725 8000 50  0000 L CNN
F 1 "100n" H 2725 7800 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 2738 7750 50  0001 C CNN
F 3 "" H 2700 7900 50  0001 C CNN
	1    2700 7900
	1    0    0    -1  
$EndComp
$Comp
L Device:C CSS1
U 1 1 5AD0B7B3
P 3200 8050
F 0 "CSS1" H 3225 8150 50  0000 L CNN
F 1 "10n" H 3225 7950 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 3238 7900 50  0001 C CNN
F 3 "" H 3200 8050 50  0001 C CNN
	1    3200 8050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5AD0BB9E
P 2900 8200
F 0 "#PWR09" H 2900 7950 50  0001 C CNN
F 1 "GND" H 2900 8050 50  0000 C CNN
F 2 "" H 2900 8200 50  0001 C CNN
F 3 "" H 2900 8200 50  0001 C CNN
	1    2900 8200
	1    0    0    -1  
$EndComp
Text Label 6850 5850 0    60   ~ 0
Supply
Text Label 5050 6300 2    60   ~ 0
Supply
$Comp
L Device:C C2
U 1 1 5AD0C260
P 3650 6350
F 0 "C2" H 3675 6450 50  0000 L CNN
F 1 "2.2u" H 3675 6250 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 3688 6200 50  0001 C CNN
F 3 "" H 3650 6350 50  0001 C CNN
	1    3650 6350
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5AD0C2DF
P 3250 6350
F 0 "R16" V 3330 6350 50  0000 C CNN
F 1 "36k" V 3250 6350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 6350 50  0001 C CNN
F 3 "" H 3250 6350 50  0001 C CNN
	1    3250 6350
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5AD0C602
P 2600 6750
F 0 "R12" V 2680 6750 50  0000 C CNN
F 1 "36k" V 2600 6750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2530 6750 50  0001 C CNN
F 3 "" H 2600 6750 50  0001 C CNN
	1    2600 6750
	0    1    1    0   
$EndComp
$Comp
L RegulatorV2-rescue:LM358 U3
U 2 1 5AD0D786
P 6950 5550
F 0 "U3" H 6950 5750 50  0000 L CNN
F 1 "LM358N" H 6950 5350 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 6950 5550 50  0001 C CNN
F 3 "" H 6950 5550 50  0001 C CNN
	2    6950 5550
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5AD0DAA8
P 6850 5250
F 0 "#PWR010" H 6850 5000 50  0001 C CNN
F 1 "GND" H 6850 5100 50  0000 C CNN
F 2 "" H 6850 5250 50  0001 C CNN
F 3 "" H 6850 5250 50  0001 C CNN
	1    6850 5250
	1    0    0    1   
$EndComp
Text Label 7600 5300 0    60   ~ 0
Vref
$Comp
L RegulatorV2-rescue:LM358 U2
U 2 1 5AD0EB4C
P 4500 4700
F 0 "U2" H 4500 4900 50  0000 L CNN
F 1 "LM358N" H 4500 4500 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 4500 4700 50  0001 C CNN
F 3 "" H 4500 4700 50  0001 C CNN
	2    4500 4700
	1    0    0    1   
$EndComp
Text Label 5200 4700 2    60   ~ 0
Vmeas
$Comp
L power:GND #PWR011
U 1 1 5AD0FD79
P 4400 4150
F 0 "#PWR011" H 4400 3900 50  0001 C CNN
F 1 "GND" H 4400 4000 50  0000 C CNN
F 2 "" H 4400 4150 50  0001 C CNN
F 3 "" H 4400 4150 50  0001 C CNN
	1    4400 4150
	1    0    0    1   
$EndComp
Text Label 4400 5000 0    60   ~ 0
Supply
$Comp
L RegulatorV2-rescue:LM358 U2
U 1 1 5AD104D2
P 9750 2750
F 0 "U2" H 9750 2950 50  0000 L CNN
F 1 "LM358N" H 9750 2550 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 9750 2750 50  0001 C CNN
F 3 "" H 9750 2750 50  0001 C CNN
	1    9750 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 5AD107DC
P 9150 2850
F 0 "R22" V 9230 2850 50  0000 C CNN
F 1 "13k" V 9150 2850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9080 2850 50  0001 C CNN
F 3 "" H 9150 2850 50  0001 C CNN
	1    9150 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5AD1088A
P 9650 3300
F 0 "R25" V 9730 3300 50  0000 C CNN
F 1 "11k" V 9650 3300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9580 3300 50  0001 C CNN
F 3 "" H 9650 3300 50  0001 C CNN
	1    9650 3300
	0    1    1    0   
$EndComp
Text Label 9100 2650 0    60   ~ 0
Vref
$Comp
L power:GND #PWR012
U 1 1 5AD1151C
P 9650 3050
F 0 "#PWR012" H 9650 2800 50  0001 C CNN
F 1 "GND" H 9650 2900 50  0000 C CNN
F 2 "" H 9650 3050 50  0001 C CNN
F 3 "" H 9650 3050 50  0001 C CNN
	1    9650 3050
	1    0    0    -1  
$EndComp
Text Label 9650 2450 0    60   ~ 0
Supply
Text Label 10250 2750 0    60   ~ 0
Vcons
$Comp
L RegulatorV2-rescue:LM358 U3
U 1 1 5AD11A82
P 1850 6750
F 0 "U3" H 1850 6950 50  0000 L CNN
F 1 "LM358N" H 1850 6550 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 1850 6750 50  0001 C CNN
F 3 "" H 1850 6750 50  0001 C CNN
	1    1850 6750
	1    0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5AD11E57
P 1100 6650
F 0 "R1" V 1180 6650 50  0000 C CNN
F 1 "10k" V 1100 6650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1030 6650 50  0001 C CNN
F 3 "" H 1100 6650 50  0001 C CNN
	1    1100 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5AD11EEA
P 1100 6850
F 0 "R2" V 1180 6850 50  0000 C CNN
F 1 "10k" V 1100 6850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1030 6850 50  0001 C CNN
F 3 "" H 1100 6850 50  0001 C CNN
	1    1100 6850
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5AD11F72
P 1400 7050
F 0 "R3" V 1480 7050 50  0000 C CNN
F 1 "10k" V 1400 7050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1330 7050 50  0001 C CNN
F 3 "" H 1400 7050 50  0001 C CNN
	1    1400 7050
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5AD12009
P 1850 6100
F 0 "R4" V 1930 6100 50  0000 C CNN
F 1 "10k" V 1850 6100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1780 6100 50  0001 C CNN
F 3 "" H 1850 6100 50  0001 C CNN
	1    1850 6100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5AD12869
P 1750 6450
F 0 "#PWR013" H 1750 6200 50  0001 C CNN
F 1 "GND" H 1750 6300 50  0000 C CNN
F 2 "" H 1750 6450 50  0001 C CNN
F 3 "" H 1750 6450 50  0001 C CNN
	1    1750 6450
	-1   0    0    1   
$EndComp
Text Label 1750 7050 0    60   ~ 0
Supply
Text Label 700  6850 0    60   ~ 0
Vcons
Text Label 700  6650 0    60   ~ 0
Vmeas
Text Label 1650 7300 2    60   ~ 0
Vref
$Comp
L RegulatorV2-rescue:IRLZ44N Q4
U 1 1 5AD46B8F
P 10150 7200
F 0 "Q4" H 10400 7350 50  0000 L CNN
F 1 "IRFB4321" H 10400 7250 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 10400 7125 50  0001 L CIN
F 3 "" H 10150 7200 50  0001 L CNN
	1    10150 7200
	1    0    0    -1  
$EndComp
$Comp
L RegulatorV2-rescue:IRLZ44N Q3
U 1 1 5AD46F3E
P 9750 7800
F 0 "Q3" H 10000 7875 50  0000 L CNN
F 1 "IRFB4321" H 10000 7800 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 10000 7725 50  0001 L CIN
F 3 "" H 9750 7800 50  0001 L CNN
	1    9750 7800
	1    0    0    -1  
$EndComp
$Comp
L RegulatorV2-rescue:IRLZ44N Q2
U 1 1 5AD47004
P 9350 8300
F 0 "Q2" H 9600 8375 50  0000 L CNN
F 1 "IRFB4321" H 9600 8300 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 9600 8225 50  0001 L CIN
F 3 "" H 9350 8300 50  0001 L CNN
F 4 "TO220" H 9350 8300 60  0001 C CNN "Package"
	1    9350 8300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R26
U 1 1 5AD47166
P 8650 7200
F 0 "R26" V 8730 7200 50  0000 C CNN
F 1 "22" V 8650 7200 50  0000 C CNN
F 2 "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal" V 8580 7200 50  0001 C CNN
F 3 "" H 8650 7200 50  0001 C CNN
	1    8650 7200
	0    1    1    0   
$EndComp
$Comp
L Device:R R27
U 1 1 5AD472D6
P 8650 7800
F 0 "R27" V 8730 7800 50  0000 C CNN
F 1 "22" V 8650 7800 50  0000 C CNN
F 2 "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal" V 8580 7800 50  0001 C CNN
F 3 "" H 8650 7800 50  0001 C CNN
	1    8650 7800
	0    1    1    0   
$EndComp
$Comp
L Device:R R28
U 1 1 5AD473AE
P 8650 8300
F 0 "R28" V 8730 8300 50  0000 C CNN
F 1 "22" V 8650 8300 50  0000 C CNN
F 2 "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal" V 8580 8300 50  0001 C CNN
F 3 "" H 8650 8300 50  0001 C CNN
	1    8650 8300
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 5AD47D18
P 8050 9200
F 0 "R24" V 8130 9200 50  0000 C CNN
F 1 "1.5k" V 8050 9200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7980 9200 50  0001 C CNN
F 3 "" H 8050 9200 50  0001 C CNN
	1    8050 9200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5AD48005
P 8050 9450
F 0 "#PWR014" H 8050 9200 50  0001 C CNN
F 1 "GND" H 8050 9300 50  0000 C CNN
F 2 "" H 8050 9450 50  0001 C CNN
F 3 "" H 8050 9450 50  0001 C CNN
	1    8050 9450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5AD4A3E4
P 9450 8500
F 0 "#PWR015" H 9450 8250 50  0001 C CNN
F 1 "GND" H 9450 8350 50  0000 C CNN
F 2 "" H 9450 8500 50  0001 C CNN
F 3 "" H 9450 8500 50  0001 C CNN
	1    9450 8500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5AD4A486
P 9850 8000
F 0 "#PWR016" H 9850 7750 50  0001 C CNN
F 1 "GND" H 9850 7850 50  0000 C CNN
F 2 "" H 9850 8000 50  0001 C CNN
F 3 "" H 9850 8000 50  0001 C CNN
	1    9850 8000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5AD4A521
P 10250 7400
F 0 "#PWR017" H 10250 7150 50  0001 C CNN
F 1 "GND" H 10250 7250 50  0000 C CNN
F 2 "" H 10250 7400 50  0001 C CNN
F 3 "" H 10250 7400 50  0001 C CNN
	1    10250 7400
	1    0    0    -1  
$EndComp
Text Label 9450 8100 0    60   ~ 0
Vdump
Text Label 9850 7600 0    60   ~ 0
Vdump
Text Label 10250 7000 0    60   ~ 0
Vdump
$Comp
L power:GND #PWR018
U 1 1 5AD4BC90
P 4650 7850
F 0 "#PWR018" H 4650 7600 50  0001 C CNN
F 1 "GND" H 4650 7700 50  0000 C CNN
F 2 "" H 4650 7850 50  0001 C CNN
F 3 "" H 4650 7850 50  0001 C CNN
	1    4650 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5AD76DA2
P 2350 4600
F 0 "R10" V 2430 4600 50  0000 C CNN
F 1 "20k" V 2350 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2280 4600 50  0001 C CNN
F 3 "" H 2350 4600 50  0001 C CNN
	1    2350 4600
	1    0    0    -1  
$EndComp
$Comp
L RegulatorV2-rescue:POT RV1
U 1 1 5AD78034
P 9900 1300
F 0 "RV1" V 9725 1300 50  0000 C CNN
F 1 "10k" V 9800 1300 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386C_Horizontal" H 9900 1300 50  0001 C CNN
F 3 "" H 9900 1300 50  0001 C CNN
	1    9900 1300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5ADE0644
P 1400 1950
F 0 "#PWR019" H 1400 1700 50  0001 C CNN
F 1 "GND" H 1400 1800 50  0000 C CNN
F 2 "" H 1400 1950 50  0001 C CNN
F 3 "" H 1400 1950 50  0001 C CNN
	1    1400 1950
	1    0    0    -1  
$EndComp
Text Label 1300 2350 0    60   ~ 0
Vdump
Text Label 3700 6650 0    60   ~ 0
Vref
$Comp
L RegulatorV2-rescue:Conn_01x03 J3
U 1 1 5ADE5491
P 2050 3000
F 0 "J3" H 2050 3200 50  0000 C CNN
F 1 "Conn_01x03" H 2050 2800 50  0000 C CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x03_P3.50mm_Horizontal" H 2050 3000 50  0001 C CNN
F 3 "" H 2050 3000 50  0001 C CNN
	1    2050 3000
	1    0    0    -1  
$EndComp
Text Label 1250 2900 0    60   ~ 0
Sensor_VDD
Text Label 1200 3000 0    60   ~ 0
Vout_sensor
Text Label 1200 3100 0    60   ~ 0
Sensor_VSS
$Comp
L power:GND #PWR020
U 1 1 5ADE6F12
P 4150 7050
F 0 "#PWR020" H 4150 6800 50  0001 C CNN
F 1 "GND" H 4150 6900 50  0000 C CNN
F 2 "" H 4150 7050 50  0001 C CNN
F 3 "" H 4150 7050 50  0001 C CNN
	1    4150 7050
	0    1    1    0   
$EndComp
Text Notes 7850 750  0    60   ~ 0
Sensor Interface\n
Text Notes 2000 1200 0    60   ~ 0
Connectors\n\n
Text Notes 2450 3850 0    60   ~ 0
Battery voltage measurement interface\n
Text Notes 2150 5900 0    60   ~ 0
Proportional Integral controller\n
Text Notes 8700 6150 0    60   ~ 0
Power interface\n
$Comp
L Device:C C4
U 1 1 5AE10082
P 7600 5450
F 0 "C4" H 7625 5550 50  0000 L CNN
F 1 "100n" H 7625 5350 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 7638 5300 50  0001 C CNN
F 3 "" H 7600 5450 50  0001 C CNN
	1    7600 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5AE10523
P 7600 5600
F 0 "#PWR021" H 7600 5350 50  0001 C CNN
F 1 "GND" H 7600 5450 50  0000 C CNN
F 2 "" H 7600 5600 50  0001 C CNN
F 3 "" H 7600 5600 50  0001 C CNN
	1    7600 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5AE148EB
P 1750 9400
F 0 "#PWR022" H 1750 9150 50  0001 C CNN
F 1 "GND" H 1750 9250 50  0000 C CNN
F 2 "" H 1750 9400 50  0001 C CNN
F 3 "" H 1750 9400 50  0001 C CNN
	1    1750 9400
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5AE14A31
P 2050 9400
F 0 "R7" V 2130 9400 50  0000 C CNN
F 1 "1k" V 2050 9400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1980 9400 50  0001 C CNN
F 3 "" H 2050 9400 50  0001 C CNN
	1    2050 9400
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5AE14B2A
P 2550 9400
F 0 "D2" H 2550 9500 50  0000 C CNN
F 1 "LED L-53GD" H 2600 9250 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 2550 9400 50  0001 C CNN
F 3 "" H 2550 9400 50  0001 C CNN
	1    2550 9400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5AE14BE3
P 2550 9850
F 0 "D3" H 2550 9950 50  0000 C CNN
F 1 "LED L-53HD" H 2600 9700 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 2550 9850 50  0001 C CNN
F 3 "" H 2550 9850 50  0001 C CNN
	1    2550 9850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5AE14D08
P 2050 9850
F 0 "R8" V 2130 9850 50  0000 C CNN
F 1 "4.7k" V 2050 9850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" V 1980 9850 50  0001 C CNN
F 3 "" H 2050 9850 50  0001 C CNN
	1    2050 9850
	0    1    1    0   
$EndComp
Text Label 1550 9850 0    60   ~ 0
Vdump
Text Label 3150 9400 0    60   ~ 0
Supply
Text Label 3150 9850 0    60   ~ 0
Vin
Text Notes 2500 9000 0    60   ~ 0
Lightning\n
Wire Wire Line
	6850 1400 6850 1800
Wire Wire Line
	8100 2850 9000 2850
Wire Wire Line
	1400 1350 1400 1650
Wire Wire Line
	1400 1650 1700 1650
Wire Wire Line
	3550 1700 3550 1800
Wire Wire Line
	4050 1700 4050 1950
Wire Wire Line
	3550 2100 3550 2150
Wire Wire Line
	3750 2150 3550 2150
Connection ~ 3550 2150
Wire Wire Line
	4050 2350 4300 2350
Wire Wire Line
	3550 2500 3550 2650
Wire Wire Line
	3550 2650 3950 2650
Connection ~ 3950 2650
Connection ~ 4300 2350
Wire Wire Line
	7450 2250 7900 2250
Wire Wire Line
	1200 4350 2050 4350
Wire Wire Line
	2050 4450 2050 4350
Connection ~ 2050 4350
Wire Wire Line
	2650 4450 2650 4350
Wire Wire Line
	2050 4900 2050 4750
Connection ~ 2350 4350
Wire Wire Line
	2650 4900 2650 4750
Wire Wire Line
	2050 5200 2050 5300
Wire Wire Line
	2350 5200 2350 5300
Wire Wire Line
	2650 5300 2650 5200
Wire Wire Line
	2050 5300 2350 5300
Connection ~ 2350 5300
Connection ~ 4650 2350
Wire Wire Line
	3000 4700 3550 4700
Wire Wire Line
	3000 4700 3000 4750
Wire Wire Line
	3000 4750 2650 4750
Wire Wire Line
	2350 4800 3550 4800
Connection ~ 2350 4800
Wire Wire Line
	2050 4900 3550 4900
Connection ~ 2650 4750
Connection ~ 2050 4900
Wire Wire Line
	7450 2200 7450 2250
Wire Wire Line
	7150 1400 7950 1400
Wire Wire Line
	7950 1400 7950 1600
Wire Wire Line
	7950 1900 7850 1900
Wire Wire Line
	7450 1600 7450 1300
Wire Wire Line
	6750 2000 7250 2000
Wire Wire Line
	6450 1800 6850 1800
Connection ~ 6850 1800
Wire Wire Line
	9450 1750 8800 1750
Wire Wire Line
	8800 1750 8800 1850
Wire Wire Line
	8800 2150 8800 2250
Wire Wire Line
	9650 1250 9650 1350
Wire Wire Line
	8800 1450 8800 1250
Wire Wire Line
	8800 1250 9000 1250
Wire Wire Line
	10050 1650 10200 1650
Wire Wire Line
	10200 1300 10200 1650
Wire Wire Line
	10050 1300 10100 1300
Wire Wire Line
	9750 1300 9450 1300
Wire Wire Line
	9450 1300 9450 1550
Connection ~ 9450 1550
Wire Wire Line
	9650 1950 9650 2200
Connection ~ 10200 1650
Wire Wire Line
	4150 7350 3150 7350
Wire Wire Line
	4150 7450 2700 7450
Wire Wire Line
	2700 7450 2700 7550
Wire Wire Line
	2700 8200 2850 8200
Wire Wire Line
	2700 8200 2700 8050
Connection ~ 2900 8200
Connection ~ 2850 8200
Wire Wire Line
	3100 6350 3000 6350
Wire Wire Line
	3000 6350 3000 6750
Wire Wire Line
	2750 6750 3000 6750
Connection ~ 3000 6750
Wire Wire Line
	6650 5450 6500 5450
Wire Wire Line
	6500 5450 6500 5000
Wire Wire Line
	6500 5000 7350 5000
Wire Wire Line
	7350 5000 7350 5550
Wire Wire Line
	7350 5550 7250 5550
Wire Wire Line
	7350 5550 7600 5300
Connection ~ 7350 5550
Wire Wire Line
	3950 4800 4000 4800
Wire Wire Line
	4200 4600 4100 4600
Wire Wire Line
	4100 4600 4100 4300
Wire Wire Line
	4100 4300 4900 4300
Wire Wire Line
	4900 4300 4900 4700
Wire Wire Line
	4800 4700 4900 4700
Connection ~ 4900 4700
Wire Wire Line
	4400 4400 4400 4150
Wire Wire Line
	9300 2850 9400 2850
Wire Wire Line
	9500 3300 9400 3300
Wire Wire Line
	9400 3300 9400 2850
Connection ~ 9400 2850
Wire Wire Line
	10050 2750 10250 2750
Wire Wire Line
	9800 3300 10050 3300
Wire Wire Line
	10050 3300 10050 2750
Connection ~ 10050 2750
Wire Wire Line
	9450 2650 9100 2650
Wire Wire Line
	2150 6750 2350 6750
Wire Wire Line
	2000 6100 2350 6250
Wire Wire Line
	2350 6250 2350 6750
Connection ~ 2350 6750
Wire Wire Line
	1700 6100 1400 6250
Wire Wire Line
	1400 6250 1400 6650
Wire Wire Line
	1250 6650 1400 6650
Connection ~ 1400 6650
Wire Wire Line
	1250 6850 1400 6850
Wire Wire Line
	1400 6850 1400 6900
Connection ~ 1400 6850
Wire Wire Line
	950  6850 700  6850
Wire Wire Line
	1400 7200 1400 7300
Wire Wire Line
	1400 7300 1650 7300
Wire Wire Line
	7750 7050 7750 7200
Wire Wire Line
	7750 7200 8050 7200
Connection ~ 7750 7200
Wire Wire Line
	8500 7800 8050 7800
Wire Wire Line
	8050 7200 8050 7800
Connection ~ 8050 7200
Wire Wire Line
	8050 8300 8500 8300
Connection ~ 8050 7800
Wire Wire Line
	8800 7200 9950 7200
Wire Wire Line
	8800 7800 9550 7800
Wire Wire Line
	8800 8300 9150 8300
Connection ~ 8050 8300
Wire Wire Line
	8050 9350 8050 9450
Wire Wire Line
	2350 4750 2350 4800
Wire Wire Line
	2350 4450 2350 4350
Wire Wire Line
	9900 1150 10100 1150
Wire Wire Line
	10100 1150 10100 1300
Connection ~ 10100 1300
Wire Wire Line
	1700 1900 1400 1900
Wire Wire Line
	1400 1900 1400 1950
Wire Wire Line
	1300 2350 1700 2350
Wire Wire Line
	1850 2900 1250 2900
Wire Wire Line
	1200 3000 1850 3000
Wire Wire Line
	1850 3100 1200 3100
Wire Notes Line
	10850 3500 5800 3500
Wire Notes Line
	5800 3500 5800 800 
Wire Notes Line
	5800 800  10850 800 
Wire Notes Line
	10850 800  10850 3500
Wire Notes Line
	2450 1150 2450 3350
Wire Notes Line
	1100 3900 5550 3900
Wire Notes Line
	5550 3900 5550 5700
Wire Notes Line
	5550 5700 1100 5700
Wire Notes Line
	1100 5700 1100 3900
Wire Wire Line
	3500 6350 3400 6350
Wire Notes Line
	650  5950 650  7400
Wire Notes Line
	700  8450 5100 8450
Wire Notes Line
	5050 7400 5050 5950
Wire Notes Line
	5050 5950 650  5950
Wire Notes Line
	7350 6200 7350 9700
Wire Notes Line
	7350 9700 11450 9700
Wire Notes Line
	11450 9700 11450 6200
Wire Notes Line
	11450 6200 7350 6200
Wire Notes Line
	1350 9050 1350 10650
Wire Notes Line
	1350 10650 4550 10650
Wire Notes Line
	4550 10650 4550 9050
Wire Notes Line
	4550 9050 1350 9050
Wire Wire Line
	1900 9400 1750 9400
Wire Wire Line
	2200 9400 2400 9400
Wire Wire Line
	1550 9850 1900 9850
Wire Wire Line
	2200 9850 2400 9850
Wire Wire Line
	2700 9400 3150 9400
Wire Wire Line
	2700 9850 3150 9850
Wire Wire Line
	10250 6800 10250 7000
$Comp
L Device:D D6
U 1 1 5AE18BED
P 10250 6650
F 0 "D6" H 10250 6750 50  0000 C CNN
F 1 "SR5100" H 10250 6550 50  0000 C CNN
F 2 "Diode_THT:D_DO-27_P15.24mm_Horizontal" H 10250 6650 50  0001 C CNN
F 3 "" H 10250 6650 50  0001 C CNN
	1    10250 6650
	0    1    1    0   
$EndComp
Wire Wire Line
	10250 6500 10250 6400
Text Label 10250 6400 0    60   ~ 0
Vin
NoConn ~ 4150 7150
NoConn ~ 4150 7250
Wire Wire Line
	950  6650 700  6650
$Comp
L power:PWR_FLAG #FLG023
U 1 1 5AE18FB6
P 4650 2150
F 0 "#FLG023" H 4650 2225 50  0001 C CNN
F 1 "PWR_FLAG" H 4650 2300 50  0000 C CNN
F 2 "" H 4650 2150 50  0001 C CNN
F 3 "" H 4650 2150 50  0001 C CNN
	1    4650 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2150 4650 2350
Wire Wire Line
	1300 2600 1700 2600
Text Label 1300 2600 0    60   ~ 0
Vin
$Comp
L RegulatorV2-rescue:IRLZ44N Q5
U 1 1 5AFDB25B
P 9650 8800
F 0 "Q5" H 9900 8875 50  0000 L CNN
F 1 "IRFB4321" H 9900 8800 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 9900 8725 50  0001 L CIN
F 3 "" H 9650 8800 50  0001 L CNN
	1    9650 8800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R29
U 1 1 5AFDB326
P 8650 8800
F 0 "R29" V 8730 8800 50  0000 C CNN
F 1 "22" V 8650 8800 50  0000 C CNN
F 2 "Resistors_perso:R_Axial_DIN0207_L40mm_D2.5mm_P15.24mm_Horizontal" V 8580 8800 50  0001 C CNN
F 3 "" H 8650 8800 50  0001 C CNN
	1    8650 8800
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 8800 8500 8800
Connection ~ 8050 8800
Wire Wire Line
	8800 8800 9450 8800
Text Label 9750 8600 0    60   ~ 0
Vdump
$Comp
L power:GND #PWR024
U 1 1 5AFDB6AA
P 9750 9000
F 0 "#PWR024" H 9750 8750 50  0001 C CNN
F 1 "GND" H 9750 8850 50  0000 C CNN
F 2 "" H 9750 9000 50  0001 C CNN
F 3 "" H 9750 9000 50  0001 C CNN
	1    9750 9000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5B327D59
P 10250 2900
F 0 "C8" H 10275 3000 50  0000 L CNN
F 1 "2.2u" H 10275 2800 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 10288 2750 50  0001 C CNN
F 3 "" H 10250 2900 50  0001 C CNN
	1    10250 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5B327E4B
P 10250 3050
F 0 "#PWR025" H 10250 2800 50  0001 C CNN
F 1 "GND" H 10250 2900 50  0000 C CNN
F 2 "" H 10250 3050 50  0001 C CNN
F 3 "" H 10250 3050 50  0001 C CNN
	1    10250 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5B32C700
P 4000 4950
F 0 "C3" H 4025 5050 50  0000 L CNN
F 1 "100n" H 4025 4850 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.7mm_W2.5mm_P5.00mm" H 4038 4800 50  0001 C CNN
F 3 "" H 4000 4950 50  0001 C CNN
	1    4000 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5B32C7E0
P 4000 5100
F 0 "#PWR026" H 4000 4850 50  0001 C CNN
F 1 "GND" H 4000 4950 50  0000 C CNN
F 2 "" H 4000 5100 50  0001 C CNN
F 3 "" H 4000 5100 50  0001 C CNN
	1    4000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1700 4050 1700
$Comp
L Device:D D5
U 1 1 5B369317
P 7500 7050
F 0 "D5" H 7500 7150 50  0000 C CNN
F 1 "1N5819" H 7500 6950 50  0000 C CNN
F 2 "Diodes_THT:D_DO-15_P10.16mm_Horizontal" H 7500 7050 50  0001 C CNN
F 3 "" H 7500 7050 50  0001 C CNN
	1    7500 7050
	-1   0    0    1   
$EndComp
$Comp
L Device:D D4
U 1 1 5B36955A
P 7500 7500
F 0 "D4" H 7500 7600 50  0000 C CNN
F 1 "1N5819" H 7500 7400 50  0000 C CNN
F 2 "Diodes_THT:D_DO-15_P10.16mm_Horizontal" H 7500 7500 50  0001 C CNN
F 3 "" H 7500 7500 50  0001 C CNN
	1    7500 7500
	-1   0    0    1   
$EndComp
Wire Wire Line
	7750 7500 7650 7500
Wire Wire Line
	7750 7050 7650 7050
Connection ~ 4000 4800
Wire Notes Line
	2450 1150 1100 1150
Wire Notes Line
	2450 3350 1100 3350
Wire Notes Line
	3200 1450 3200 3100
Wire Notes Line
	3200 3100 5100 3100
Wire Notes Line
	5100 3100 5100 1450
Wire Notes Line
	5100 1450 3200 1450
Text Notes 3800 1550 0    60   ~ 0
Power supply\n\n\n
Wire Wire Line
	3550 2150 3550 2200
Wire Wire Line
	3950 2650 4300 2650
Wire Wire Line
	4300 2350 4650 2350
Wire Wire Line
	2050 4350 2350 4350
Wire Wire Line
	2350 4350 2650 4350
Wire Wire Line
	2350 5300 2350 5400
Wire Wire Line
	2350 5300 2650 5300
Wire Wire Line
	4650 2350 4700 2350
Wire Wire Line
	2350 4800 2350 4900
Wire Wire Line
	6850 1800 7250 1800
Wire Wire Line
	2900 8200 3200 8200
Wire Wire Line
	2850 8200 2900 8200
Wire Wire Line
	4900 4700 5200 4700
Wire Wire Line
	9400 2850 9450 2850
Wire Wire Line
	2350 6750 2450 6750
Wire Wire Line
	1400 6650 1550 6650
Wire Wire Line
	1400 6850 1550 6850
Wire Wire Line
	7750 7200 7750 7500
Wire Wire Line
	8050 7200 8500 7200
Wire Wire Line
	8050 7800 8050 8300
Wire Wire Line
	8050 8300 8050 8800
Wire Wire Line
	10100 1300 10200 1300
Wire Wire Line
	8050 8800 8050 9050
Wire Wire Line
	4000 4800 4200 4800
Wire Notes Line
	1100 1150 1100 3350
Wire Wire Line
	2850 7350 2850 8200
Wire Wire Line
	4150 7550 3700 7550
Wire Wire Line
	3400 7550 2700 7550
Connection ~ 2700 7550
Wire Wire Line
	2700 7550 2700 7750
Wire Wire Line
	3200 7900 3200 7650
Wire Wire Line
	3200 7650 4150 7650
Wire Wire Line
	4150 6650 3700 6650
Wire Wire Line
	3000 6750 4150 6750
Wire Wire Line
	4150 6850 3950 6850
Wire Wire Line
	3950 6850 3950 6350
Wire Wire Line
	3950 6350 3800 6350
Wire Wire Line
	5150 7500 5150 7250
Wire Wire Line
	5050 6300 4750 6300
Wire Wire Line
	4750 6300 4750 6450
Wire Wire Line
	4650 6450 4650 6300
Wire Wire Line
	4650 6300 4750 6300
Connection ~ 4750 6300
Wire Wire Line
	4150 6950 4050 6950
Wire Wire Line
	4050 6950 4050 5650
Wire Wire Line
	4050 5650 5900 5650
Wire Wire Line
	10200 1650 10750 1650
Wire Wire Line
	8500 1600 7950 1600
Connection ~ 7950 1600
Wire Wire Line
	7950 1600 7950 1900
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5EF021C0
P 5900 5500
F 0 "#FLG0101" H 5900 5575 50  0001 C CNN
F 1 "PWR_FLAG" H 5900 5674 50  0000 C CNN
F 2 "" H 5900 5500 50  0001 C CNN
F 3 "~" H 5900 5500 50  0001 C CNN
	1    5900 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5500 5900 5650
Connection ~ 5900 5650
Wire Wire Line
	5900 5650 6650 5650
$Comp
L Mechanical:MountingHole_Pad MH3
U 1 1 5EF185DA
P 1800 2350
F 0 "MH3" V 1754 2500 50  0000 L CNN
F 1 "MountingHole_Pad" V 1845 2500 50  0000 L CNN
F 2 "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad" H 1800 2350 50  0001 C CNN
F 3 "~" H 1800 2350 50  0001 C CNN
	1    1800 2350
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH1
U 1 1 5EF18686
P 1800 1650
F 0 "MH1" V 1754 1800 50  0000 L CNN
F 1 "MountingHole_Pad" V 1845 1800 50  0000 L CNN
F 2 "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad" H 1800 1650 50  0001 C CNN
F 3 "~" H 1800 1650 50  0001 C CNN
	1    1800 1650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH2
U 1 1 5EF18720
P 1800 1900
F 0 "MH2" V 1754 2050 50  0000 L CNN
F 1 "MountingHole_Pad" V 1845 2050 50  0000 L CNN
F 2 "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad" H 1800 1900 50  0001 C CNN
F 3 "~" H 1800 1900 50  0001 C CNN
	1    1800 1900
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH4
U 1 1 5EF187B4
P 1800 2600
F 0 "MH4" V 1754 2750 50  0000 L CNN
F 1 "MountingHole_Pad" V 1845 2750 50  0000 L CNN
F 2 "MountingHole:MountingHole_5.3mm_M5_DIN965_Pad" H 1800 2600 50  0001 C CNN
F 3 "~" H 1800 2600 50  0001 C CNN
	1    1800 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 7500 7350 7500
Wire Wire Line
	5150 7050 7350 7050
$EndSCHEMATC
